package com.hcl.mvc.controller;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.portlet.ModelAndView;

import com.hcl.mvc.Service.DemoService;

//@RestController
@Controller
public class HomeCon {
	
	@Autowired
	DemoService ds;
	
	@RequestMapping("/hello")
	public String hello()
	{
		System.out.println("hello at mvc");
		return "/view/home.jsp";
	}
	
	@RequestMapping("/flash")
	public String flash(ModelMap mv)
	{
		System.out.println("bye falsh");
//		ModelAndView mv=new ModelAndView();
		mv.addAttribute("name", ds.name());
		mv.addAttribute("realname", ds.RealName());
//		mv.setViewName("/view/home.jsp");
		
		return "/view/home.jsp";
		
	}
	
	
	@RequestMapping("/data")
	@ResponseBody
	public String flash(int no)
	{
		String data="";
		for(int i=1;i<=10;i++)
		{
			data=data+(no+"X"+i+"="+(no*i)+"<br>");
		}
		
		return data;
		
	}
	
	@RequestMapping("/flash2/{name}/{rel}")
	public String flash2(@PathVariable(value = "name") String name,@PathVariable(value = "rel") String rel,ModelMap mv)
	{
		System.out.println("bye falsh2");
//		ModelAndView mv=new ModelAndView();
		mv.addAttribute("name", name);
		mv.addAttribute("realname", rel);
//		mv.setViewName("/view/home.jsp");
		
		return "/view/home.jsp";
		
	}

}
